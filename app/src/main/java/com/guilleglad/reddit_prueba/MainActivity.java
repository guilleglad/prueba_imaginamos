package com.guilleglad.reddit_prueba;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView listado;
    List<Item> listado_array;
    ItemAdapter listado_adapter;
    FloatingActionButton fab;
    SharedPreferences json_persistent;
    ProgressBar pb_loading;

    @Override
    protected void onResume() {
        super.onResume();
        String json_response;
        json_persistent = this.getSharedPreferences(Constants.SP_NAME,MODE_PRIVATE);
        json_response = json_persistent.getString("JSON","");
        if(!json_response.isEmpty())
            try {
                procesar_response(json_response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(R.string.titulo);
        getSupportActionBar().setIcon(R.mipmap.reddit_logo);
        iniciar_componentes();
        iniciar_eventos();

    }

    private void iniciar_peticion() {
        if(!chequear_conexion()) {
            Toast.makeText(this, getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
        StringRequest json_request = new StringRequest(Request.Method.GET, Constants.json_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pb_loading.setVisibility(View.INVISIBLE);
                if(response.isEmpty())
                    return;
                System.out.print(response);
                guardar_response(response);
                try {
                    procesar_response(response);
                    System.out.println("OK");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error:" + error.toString());
            }
        });
        pb_loading.setVisibility(View.VISIBLE);
        AppController.getInstance().addToRequestQueue(json_request,"JSON_REQUEST");
    }

    private boolean chequear_conexion() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void guardar_response(String response) {
        json_persistent = this.getSharedPreferences(Constants.SP_NAME,MODE_PRIVATE);
        SharedPreferences.Editor _editor = json_persistent.edit();
        _editor.clear();
        _editor.putString("JSON",response);
        _editor.commit();
    }

    public void procesar_response(String response) throws JSONException {
        JSONObject json_response = new JSONObject(response);
        JSONObject json_data = new JSONObject(json_response.getString("data"));
        JSONArray json_children = new JSONArray(json_data.getString("children"));
        listado_array.clear();
        listado_adapter.notifyDataSetChanged();

        for(int i=0;i<json_children.length();i++){
            JSONObject obj = new JSONObject(json_children.getString(i));
            JSONObject obj_data = new JSONObject(obj.getString("data"));
            Item item_temp = new Item(
                    obj_data.getString("banner_img"),
                    obj_data.getString("id"),
                    obj_data.getString("header_img"),
                    obj_data.getString("display_name"),
                    obj_data.getString("icon_img"),
                    obj_data.getString("header_title"),
                    obj_data.getString("description_html"),
                    obj_data.getString("url"),
                    obj_data.getString("advertiser_category"),
                    obj_data.getString("submit_text_html")
            );
            listado_array.add(item_temp);
        }
        listado_adapter.notifyDataSetChanged();
        listado.scrollToPosition(0);
    }

    private void iniciar_eventos() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iniciar_peticion();
            }
        });
    }

    private void iniciar_componentes() {
        //LISTADO
        listado = (RecyclerView)findViewById(R.id.listado_reddits);
        listado_array = new ArrayList<Item>();
        listado_adapter = new ItemAdapter(listado_array,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listado.setLayoutManager(mLayoutManager);
        listado.setItemAnimator(new DefaultItemAnimator());
        listado.setAdapter(listado_adapter);
        //BOTONES
        fab = (FloatingActionButton) findViewById(R.id.fab);
        //OTROS
        pb_loading = (ProgressBar)findViewById(R.id.pb_loading);
    }

}
