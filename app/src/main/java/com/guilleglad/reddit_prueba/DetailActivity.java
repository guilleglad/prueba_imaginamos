package com.guilleglad.reddit_prueba;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.github.florent37.viewanimator.ViewAnimator;

public class DetailActivity extends AppCompatActivity {
    String banner_img,
            id,
            header_img,
            title,
            icon_img,
            header_title,
            description,
            url,
            advertiser_category,
            public_description;
    TextView detail_title;
    WebView detail_description;
    SimpleDraweeView detail_img;
    SimpleDraweeView item_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.INVISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.reddit_logo);
        iniciar_componentes();
        animar();
    }

    private void animar() {
        ViewAnimator
                .animate(detail_img)
                .scale(100,1)
                .duration(1000)
                .start();
        ViewAnimator
                .animate(item_icon)
                .scale(100,1)
                .duration(1000)
                .start();
        ViewAnimator
                .animate(detail_description)
                .alpha(0,1)
                .duration(1000)
                .start();
    }

    private void iniciar_componentes() {
        detail_title = (TextView)findViewById(R.id.detail_title);
        detail_description = (WebView)findViewById(R.id.detail_description);
        detail_img = (SimpleDraweeView)findViewById(R.id.detail_img);
        item_icon = (SimpleDraweeView)findViewById(R.id.item_icon);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            init_values(extras);
            cargar_componentes();
        }
    }

    private void cargar_componentes() {
        detail_title.setText(this.title);
        detail_description.loadData(Html.fromHtml(this.description).toString()+Html.fromHtml(this.public_description).toString(),"text/html; charset=utf-8", "UTF-8");
        detail_img.setImageURI(this.header_img);
        item_icon.setImageURI(this.icon_img);
    }

    private void init_values(Bundle extras) {
        this.banner_img = extras.getString("banner_img");
        this.id = extras.getString("id");
        this.header_img = extras.getString("header_img");
        this.title = extras.getString("title");
        this.icon_img = extras.getString("icon_img");
        this.header_img = extras.getString("header_img");
        this.description = extras.getString("description");
        this.url = extras.getString("url");
        this.advertiser_category = extras.getString("advertiser_category");
        this.public_description = extras.getString("public_description");

    }

}
