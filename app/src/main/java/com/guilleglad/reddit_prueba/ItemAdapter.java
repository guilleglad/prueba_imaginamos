package com.guilleglad.reddit_prueba;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * Created by Guillermo García on 19-02-2017.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private List<Item> items;
    MainActivity _myActivity;
    private int lastPosition = -1;

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        SimpleDraweeView Icon,_tempDrawee;
        TextView Titulo;
        TextView Categoria;
        LinearLayout _layout,container;
        MainActivity _myActivity;


        String banner_img,
                id,
                header_img,
                title,
                icon_img,
                header_title,
                description,
                url,
                advertiser_category,
                public_description;


        public ItemViewHolder(View itemView, MainActivity _myActivity) {
            super(itemView);
            Icon = (SimpleDraweeView) itemView.findViewById(R.id.item_icon);
            Titulo = (TextView)itemView.findViewById(R.id.item_titulo);
            Categoria = (TextView)itemView.findViewById(R.id.item_category);
            _layout = (LinearLayout)itemView.findViewById(R.id.item_layout);
            container = (LinearLayout)itemView.findViewById(R.id.item_container);
            _tempDrawee = (SimpleDraweeView)itemView.findViewById(R.id._tempDrawee);
            this._myActivity = _myActivity;
            itemView.setOnClickListener(this);
        }

        public LinearLayout getContainer() {
            return container;
        }

        @Override
        public void onClick(View v) {
            Intent I = new Intent(this._myActivity,DetailActivity.class);
            I.putExtra("banner_img",banner_img);
            I.putExtra("id",this.id);
            I.putExtra("header_img",header_img);
            I.putExtra("title",this.title);
            I.putExtra("icon_img",icon_img);
            I.putExtra("header_title",this.header_title);
            I.putExtra("description",description);
            I.putExtra("url",this.url);
            I.putExtra("advertiser_category",advertiser_category);
            I.putExtra("public_description",this.public_description);
            this._myActivity.startActivity(I);
        }
    }

    public ItemAdapter(List<Item> items,MainActivity _act) {
        this.items = items;
        this._myActivity = _act;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemlayout,parent,false);
        return new ItemViewHolder(v,_myActivity);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        String icon_url = items.get(position).getIcon_img();
        if(icon_url.isEmpty())
            icon_url = Constants.no_pic;
        holder.Icon.setImageURI(Uri.parse(icon_url));
        holder.Titulo.setText(items.get(position).getTitle());
        String cat = items.get(position).getAdvertiser_category();
        if(!cat.isEmpty() && !cat.equals("null"))
            holder.Categoria.setText(cat);
        else
            holder.Categoria.setText("");
        holder._tempDrawee.setImageURI(Uri.parse(items.get(position).getBanner_img()));
        holder.banner_img = items.get(position).getBanner_img();
        holder.id = items.get(position).getId();
        holder.header_img = items.get(position).getHeader_img();
        holder.title = items.get(position).getTitle();
        holder.icon_img = items.get(position).getIcon_img();
        holder.header_title = items.get(position).getHeader_title();
        holder.description = items.get(position).getDescription();
        holder.url = items.get(position).getUrl();
        holder.advertiser_category = items.get(position).getAdvertiser_category();
        holder.public_description = items.get(position).getPublic_description();
        setAnimation(holder.getContainer(), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(_myActivity, R.anim.up_front_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }else{
            Animation animation = AnimationUtils.loadAnimation(_myActivity, R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
        }
    }
}
