package com.guilleglad.reddit_prueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.github.florent37.viewanimator.ViewAnimator;

public class SplashActivity extends AppCompatActivity {

    Button btn_siguiente;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setTitle(getResources().getString(R.string.titulo));
        iniciar_componentes();
        iniciar_eventos();
        animar();
    }

    private void animar() {
        ViewAnimator
                .animate(icon)
                .alpha(0,1)
                .duration(1000)
                .start();
        ViewAnimator
                .animate(btn_siguiente)
                .pulse()
                .duration(400)
                .repeatCount(-1)
                .start();
    }

    private void iniciar_componentes(){
        btn_siguiente = (Button)findViewById(R.id.btn_siguiente);
        icon = (ImageView)findViewById(R.id.icon);
    }
    private void iniciar_eventos(){
        btn_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(I);
                finish();
            }
        });
    }
}
