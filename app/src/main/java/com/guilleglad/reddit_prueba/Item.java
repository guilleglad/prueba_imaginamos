package com.guilleglad.reddit_prueba;

/**
 * Created by Guillermo García on 19-02-2017.
 */

public class Item {
    private String banner_img,
            id,
            header_img,
            title,
            icon_img,
            header_title,
            description,
            url,
            advertiser_category,
            public_description;
    private int banner_img_id,
            header_img_id,
            icon_img_id;
    public Item(String banner_img,
                String id,
                String header_img,
                String title,
                String icon_img,
                String header_title,
                String description,
                String url,
                String advertiser_category,
                String public_description){
        setBanner_img(banner_img);
        setId(id);
        setHeader_img(header_img);
        setTitle(title);
        setIcon_img(icon_img);
        setHeader_title(header_title);
        setDescription(description);
        setUrl(url);
        setAdvertiser_category(advertiser_category);
        setPublic_description(public_description);
    }

    public String getBanner_img() {
        return banner_img;
    }

    public void setBanner_img(String banner_img) {
        this.banner_img = banner_img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeader_img() {
        return header_img;
    }

    public void setHeader_img(String header_img) {
        this.header_img = header_img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon_img() {
        return icon_img;
    }

    public void setIcon_img(String icon_img) {
        this.icon_img = icon_img;
    }

    public String getHeader_title() {
        return header_title;
    }

    public void setHeader_title(String header_title) {
        this.header_title = header_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAdvertiser_category() {
        return advertiser_category;
    }

    public void setAdvertiser_category(String advertiser_category) {
        this.advertiser_category = advertiser_category;
    }

    public String getPublic_description() {
        return public_description;
    }

    public void setPublic_description(String public_description) {
        this.public_description = public_description;
    }
}
