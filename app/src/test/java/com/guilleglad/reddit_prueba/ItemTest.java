package com.guilleglad.reddit_prueba;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Guillermo García on 20-02-2017.
 */
public class ItemTest {
    Item I;
    public ItemTest(){
        I = new Item("banner.jpg","a100","header.jpg","Titulo","icon.jpg","Titulo Cabecera","Descripcion","URL","Category","Descripcion Publica");
    }
    @Test
    public void getBanner_img() throws Exception {
        assertEquals("El banner_img no es igual","banner.jpg",I.getBanner_img());
    }
    @Test
    public void getId() throws Exception {
        assertEquals("El id no es igual","a100",I.getId());
    }

    @Test
    public void getHeader_img() throws Exception {
        assertEquals("El header_img no es igual","header.jpg",I.getHeader_img());
    }

    @Test
    public void getTitle() throws Exception {
        assertEquals("El title no es igual","Titulo",I.getTitle());
    }

    @Test
    public void getIcon_img() throws Exception {
        assertEquals("El icon_img no es igual","icon.jpg",I.getIcon_img());
    }

    @Test
    public void getHeader_title() throws Exception {
        assertEquals("El header_title no es igual","Titulo Cabecera",I.getHeader_title());
    }

    @Test
    public void getDescription() throws Exception {
        assertEquals("El description no es igual","Descripcion",I.getDescription());
    }

    @Test
    public void getUrl() throws Exception {
        assertEquals("El url no es igual","URL",I.getUrl());
    }

    @Test
    public void getAdvertiser_category() throws Exception {
        assertEquals("El advertiser_category no es igual","Category",I.getAdvertiser_category());
    }

    @Test
    public void getPublic_description() throws Exception {
        assertEquals("El public_description no es igual","Descripcion Publica",I.getPublic_description());
    }
}